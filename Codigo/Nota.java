/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto;

import java.io.Serializable;

/**
 * Contem informação de uma Nota. 
 * Esta informação é constituida por uma nota(double).
 * @author João Clara & Paulo Coito;
 */
public class Nota implements Serializable{
    double nota;
    /**
     * Inicializador da nota
     * @param nota (double) Nota.
     * Atribui os parametros de entrada às respetivas variáveis.
     */
    public Nota(double nota){
        this.nota = nota;
    }
    /**
     * Permite obter a nota.
     * @return nota.
     */
    public double get_nota()
    {
        return this.nota;
    }
}
