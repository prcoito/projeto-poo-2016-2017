package projeto;

import java.util.ArrayList;
import projeto.Funcionario;

/**
 * Contem informação de um Docente. 
 * Esta informação é composta por uma area de investigação(String) e pela informação da classe Funcionario.
 * O Docente deriva de um funcionario, pelo que herda todos os seus metodos e variaveis.
 * @author João Clara & Paulo Coito;
 */
public class Docente extends Funcionario{
    String area_investigacao;
    /**
     * Inicalizador do Docente.
     * @param nome (String) Nome do Docente.
     * @param email (String) Email do Docente.
     * @param num (int) Numero mecanográfico do Docente.
     * @param cat (String) Categoria do Docente.
     * @param area (String) Area de Investigação.
     * Atribui os parametros de entrada às respetivas variáveis. Inicializa todos os ArrayList's.
     */
    public Docente(String nome, String email, int num, String cat, String area)
    {
        super(nome, email, num, cat);
        this.area_investigacao = area;
    }
     /**
     * Verifica a disponibilidade de um docente numa determinada data.
     * Dado um exame, verifica se esse exame pode ser vigiado pelo docente.
     * É percorrido o ArrayList de datas em que o docente está ocupado, e verifica se a data do exame a adicionar se sobrepõe a alguma data.
     * @param e (Exame) Exame a adicionar.
     * @return boolean. Se o docente estiver disponivel retorna true. Caso contrário retorna false.
     */
    public boolean verifica_disponibilidade(Exame e)
    {
        Data d = e.data;
        double duracao = e.duracao;
        double fPart = (duracao % 1);
        short iPart = (short) (duracao - fPart);
        
        int min = (int)(60*fPart) ;
        int fim_exame_a_adicionar = d.hora + (int)( 100*iPart + min);
        if((fim_exame_a_adicionar%100) > 60)
        {
            fim_exame_a_adicionar = fim_exame_a_adicionar-60;   // fica com os minutos bem
            fim_exame_a_adicionar = fim_exame_a_adicionar+100;  // aumenta 1 hora
        }
        int inicio_exame_a_adicionar = d.hora;
        
        for (Exame exam : this.exame) {
            if(exam.data.ano == d.ano && exam.data.dia == d.dia && exam.data.mes == d.mes)
            {
                duracao = e.duracao;
                fPart = (duracao % 1);
                iPart = (short) (duracao - fPart);

                min = (int)(60*fPart) ;
                int fim_exame_atual = exam.data.hora +(int)( 100*iPart + min);
                if((fim_exame_atual%100) > 60)
                {
                    fim_exame_atual = fim_exame_atual-60;   // fica com os minutos bem
                    fim_exame_atual = fim_exame_atual+100;  // aumenta 1 hora
                }
                  // inicio_exame_atual
                if(! (exam.data.hora > fim_exame_a_adicionar || fim_exame_atual<inicio_exame_a_adicionar) )    
                    return false;
            }
        }
        return true;  
    }
}
