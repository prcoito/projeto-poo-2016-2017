package projeto;

import java.util.ArrayList;

/**
 * Contem informação de um Funcionario. 
 * Esta informação é composta por um numero mecanográfico, uma categoria e por um ArrayList de Exames.
 * O Funcionario deriva da classe Pessoa, pelo que herda todos os seus metodos e variaveis.
 * @author João Clara & Paulo Coito;
 */
public class Funcionario extends Pessoa{
    int num_mecanografico;
    String categoria;
    ArrayList<Exame> exame;
    /**
     * Inicalizador do Funcionario.
     * @param nome (String) Nome do Funcionario.
     * @param email (String) Email do Funcionario.
     * @param num_mec (int) Numero mecanográfico do Funcionario.
     * @param cat (String) Categoria do Funcionario.
     * Atribui os parametros de entrada às respetivas variáveis. Inicializa todos os ArrayList's.
     */
    public Funcionario(String nome, String email, int num_mec, String cat){
        super(nome,email);
        this.num_mecanografico = num_mec;
        this.categoria = cat;
        exame = new ArrayList<Exame>();
    }
    /**
     * Permite listar todos os exames do Funcionario.
     */
    public void listar_exames(){
        if(exame.isEmpty())
        {
            System.out.println("Não tem nenhum exame a vigiar.");
            return;
        }
        System.out.println("Lista de exames:");
        for (Exame exame1 : this.exame) {
            System.out.print("\t-"+exame1);
        }
            
    }
    @Override
    public String toString(){
        return String.format("Nome: "+this.nome+"; Num. Mec.: "+this.num_mecanografico);
    }
}
