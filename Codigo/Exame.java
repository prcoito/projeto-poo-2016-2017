package projeto;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Contem informação de um Exame. 
 * Esta informação é composta por uma data, uma duracao(o tempo que o exame demora), por um Docente responsavel, uma disciplina, um ArrayList de salas, ArrayList de vigilantes (Docentes), um ArrayList nao docentes, para apoio ao exame e um ArrayList de alunos inscritos.
 * @author João Clara & Paulo Coito;
 */
public class Exame implements Serializable{
    Data data;
    Double duracao;
    Docente responsavel;
    Disciplina disciplina;
    ArrayList<Sala> salas;
    ArrayList<Docente> docentes;
    ArrayList<NaoDocente> naodocentes;
    ArrayList<Aluno> alunos;
    /**
     * Inicializador de um exame
     * @param disciplina (Disciplina) Disciplina do exame.
     * @param data (Data) Data do exame.
     * @param duracao (double) Duração do exame.
     * @param responsavel (Docente) Docente reponsável do exame.
     * Atribui os parametros de entrada às respetivas variáveis.
     */
    public Exame(Disciplina disciplina, Data data, double duracao, Docente responsavel){
        this.disciplina = disciplina;
        this.data = data;
        this.duracao = duracao;
        this.responsavel = responsavel;
        this.salas = new ArrayList<Sala>();
        this.docentes = new ArrayList<Docente>();
        this.naodocentes = new ArrayList<NaoDocente>();
        this.alunos = new ArrayList<Aluno>();
    }
    /**
     * Permite saber qual o tipo de exame.
     * @return (String) Tipo de exame.
     */
    public String tipo()
    {
        return "Geral";
    }
    /**
     * Permite adicionar um vigilante ao exame.
     * @param d (Docente) Docente a adicionar à vigilância do exame.
     */
    public void add_vigilante(Docente d){
        this.docentes.add(d);
    }
    /**
     * Permite adicionar uma sala ao exame.
     * @param s (Sala) Sala a adicionar.
     */
    public void add_sala(Sala s){
        this.salas.add(s);
    }
    /**
     * Permite inscrever um aluno no exame.
     * O aluno é inserido no ArrayList de alunos por ordem alfabetica.
     * @param a (Aluno) Aluno a inscrever.
     */
    public void inscrever_aluno(Aluno a)
    {
        for (int i=0; i<alunos.size();i++)   // add aluno por ordem alfabetica
        {
            if(a.nome.compareTo(alunos.get(i).nome)<0)
            {
                this.alunos.add(a);
                return;
            }
        }
        this.alunos.add(a); // se nao encontrar poe no fim da lista
    }
    
    /**
     * Lista todos os alunos inscritos no exame.
     * Imprime apenas o nome e numero do aluno.
     * @return numero total de alunos inscritos no exame.
     */
    public int listar_alunos()
    {
        int i = 0;
        if(this.alunos.isEmpty())
            System.out.println("Nao existem alunos inscritos no exame");
        
        for(i= 0; i<this.alunos.size(); i++)
            System.out.println(i+1+" - "+this.alunos.get(i).nome);
        return i;
    }
    /**
     * Permite listar todos os docentes convocados para a vigilancia do exame.
     * Imprime apenas o nome do docente. Se não houver nao docentes convocados imprime essa informação.
     * @return o numero total de docentes convocados para a vigilancia do exame.
     */
    public int listar_docentes()
    {
        int i;
        if(this.docentes.isEmpty())
        {
            System.out.println("Não existem Docentes para esta disciplina\n");
            return 0;
        }
        System.out.print("Lista de Docentes:\n");
        for (i = 0; i<this.docentes.size(); i++) {
            System.out.println(i+1+" - "+this.docentes.get(i).getName()+";");
        }
        return i;        
    }
    /**
     * Permite listar todos os nao docentes convocados para o apoio ao exame.
     * Imprime apenas o nome do nao docente. Se não houver nao docentes convocados imprime essa informação.
     * @return o numero total de nao docentes convocados para o apoio ao exame.
     */
    public int listar_naodocentes()
    {
        int i;
        if(this.naodocentes.isEmpty())
        {
            System.out.println("Não existem NaoDocentes para esta disciplina\n");
            return 0;
        }
        System.out.print("Lista de NaoDocentes:\n");
        for (i = 0; i<this.naodocentes.size(); i++) {
            System.out.println(i+1+" - "+this.naodocentes.get(i).getName()+";");
        }
        return i;        
    }
    /**
     * Permite listar as classificações do exame.
     * Imprime apenas a classificação dos alunos inscritos no exame.
     */
    public void listar_notas()
    {
        if(this.alunos.isEmpty())
            System.out.println("Não há alunos iscritos no exame.");
        for(int i = 0; i<this.alunos.size(); i++)
        {
            System.out.println(i+1+"- "+this.alunos.get(i).get_nota(this));
        }
    }
    /**
     * Permite listar a pauta do exame.
     * Imprime a lista de aluno inscritos no exame e respetiva classificação.
     */
    public void listar_pauta()
    {
        if(this.alunos.isEmpty())
            System.out.println("Não há alunos iscritos no exame.");
        for(int i = 0; i<this.alunos.size(); i++)
        {
            System.out.println(i+1+"- "+this.alunos.get(i).nome+" Nota: "+this.alunos.get(i).get_nota(this));
        }
    }
    /**
     * Permite adidionar um nao docente ao ArrayList de NaoDocentes.
     * O nao docente a inserir é colocado no fim do ArrayList.
     * @param nd (NaoDocente) NaoDocente a adicionar;
     */
    public void add_nao_docente(NaoDocente nd)
    {
        this.naodocentes.add(nd);
    }
    /**
     * Permite imprimir a informação de um exame.
     * Imprime o tipo de exame, a sua disciplina e o dia em que se realiza.
     * @return String formatada.
     */
    @Override
    public String toString()
    {
        return String.format("Exame "+this.tipo()+" de "+this.disciplina+" no dia "+this.data);
    }
}

class Ex_Normal extends Exame{
    /**
     * Inicializador de um exame
     * @param disciplina (Disciplina) Disciplina do exame.
     * @param data (Data) Data do exame.
     * @param duracao (double) Duração do exame.
     * @param responsavel (Docente) Docente reponsável do exame.
     * Atribui os parametros de entrada às respetivas variáveis.
     */
    public Ex_Normal(Disciplina disci, Data data, double durac, Docente doc_resp)
    {
        super(disci, data, durac, doc_resp);
    }
    /**
     * Permite saber qual o tipo de exame.
     * @return (String) Tipo de exame.
     */
    @Override
    public String tipo()
    {
        return "Normal";
    }
}

class Ex_Recurso extends Exame{
    /**
     * Inicializador de um exame
     * @param disciplina (Disciplina) Disciplina do exame.
     * @param data (Data) Data do exame.
     * @param duracao (double) Duração do exame.
     * @param responsavel (Docente) Docente reponsável do exame.
     * Atribui os parametros de entrada às respetivas variáveis.
     */
    public Ex_Recurso(Disciplina disci, Data data, double durac, Docente doc_resp){
        super(disci, data, durac, doc_resp);
    }
    /**
     * Permite saber qual o tipo de exame.
     * @return (String) Tipo de exame.
     */
    @Override
    public String tipo()
    {
        return "Recurso";
    }
}

class Ex_Ep_Especial extends Exame{
    /**
     * Inicializador de um exame
     * @param disciplina (Disciplina) Disciplina do exame.
     * @param data (Data) Data do exame.
     * @param duracao (double) Duração do exame.
     * @param responsavel (Docente) Docente reponsável do exame.
     * Atribui os parametros de entrada às respetivas variáveis.
     */
    public Ex_Ep_Especial(Disciplina disci, Data data, double durac, Docente doc_resp){
        super(disci, data, durac, doc_resp);
    }
    
    /**
     * Permite inscrever um aluno no exame de época especial.
     * O aluno é inserido no ArrayList de alunos por ordem alfabetica.
     * Apenas inscreve o aluno caso o aluno tenha estatuto de especial.
     * @param a (Aluno) Aluno a inscrever.
     */
    @Override
    public void inscrever_aluno(Aluno a)
    {
        if (a.especial())
        {
            for (int i=0; i<alunos.size();i++)   // add aluno por ordem alfabetica
            {
                if(a.nome.compareTo(alunos.get(i).nome)<0)
                {
                    this.alunos.add(a);
                    return;
                }
            }
            this.alunos.add(a); // se nao encontrar poe no fim da lista
        }
        else
            System.out.print("Nao é possivel inscrever esse aluno neste exame\nO aluno não tem estatuto especial");
    }
    /**
     * Permite saber qual o tipo de exame.
     * @return (String) Tipo de exame.
     */
    @Override
    public String tipo()
    {
        return "Especial";
    }
        
}
