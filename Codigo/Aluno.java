package projeto;

import java.util.ArrayList;

/**
 * Contem informação de um Aluno. 
 * Esta informação é composta pelo numero de aluno, o ano de matricula, o curso que frequenta, o regime do aluno, se o aluno tem regime especial, um ArrayList de disciplinas , um ArrayList de exames e um ArrayList de notas.
 * @author João Clara & Paulo Coito;
 */
public class Aluno extends Pessoa{
    ArrayList<Disciplina> disciplinas;
    ArrayList<Exame> exames;
    ArrayList<Nota> notas;
    int num_aluno, ano_matricula;
    Curso curso;
    String regime;
    boolean especial;
    /**
     * Inicalizador do Aluno.
     * @param nome (String) Nome do Aluno.
     * @param email (String) Email do Aluno.
     * @param num_aluno (int) Numero do Aluno.
     * @param nome_curso (Curso) Curso que o Aluno frequenta.
     * @param ano (int) Ano em que aluno começou o curso.
     * @param regime (String) Regime do Aluno.
     * Atribui os parametros de entrada às respetivas variáveis. Inicializa todos os ArrayList's.
     */
    public Aluno(String nome, String email, int num_aluno, Curso nome_curso, int ano, String regime){
        super(nome, email);
        this.num_aluno = num_aluno;
        this.curso = nome_curso;
        this.ano_matricula = ano;
        this.regime = regime.toLowerCase();
        this.especial = this.regime.compareTo("normal")!=0; // se for == true senao false
        disciplinas = new ArrayList<Disciplina>();
        exames = new ArrayList<Exame>();
        notas = new ArrayList<Nota>();
    }
    /**
     * Permite adicionar um exame a aluno.
     * Primeiro verifica se aluno está inscrito à disciplina do exame a adicionar.
     * De seguida verifica se aluno já está inscrito ao exame a adicionar.
     * Se ainda não estiver inscrito, e se for elegivel para esse exame, adiciona o exame ao ArrayList de exames do aluno.
     * @param e (Exame) Exame a adicionar.
     * @return true caso tenha sido adicionado o exame e false caso contrário.
     */
    public boolean add_exame(Exame e)
    {
        /*System.out.println(e.disciplina);
        for(int i = 0; i< this.exames.size() ; i++)
            System.out.print(this.exames.get(i));*/
        if(this.disciplinas.contains(e.disciplina))
        {
            if(this.exames.contains(e))
            {
                System.out.println("O aluno já se encontra inscrito a esse exame");
                return false;
            }
            if(e.tipo().compareTo("Especial") == 0)
            {
                if(this.especial())
                {
                    exames.add(e);// se ainda nao foi adicionado e for "especial"
                    notas.add(new Nota(0));
                    return true;
                }
                else
                {
                    System.out.println("O aluno não pode ser inscrito a esse exame\nO aluno não tem estato de especial");
                    return false;
                }
            }  
            else{
                exames.add(e);
                notas.add(new Nota(0));
                return true;
            }
        }
        else
        {
            System.out.println("O aluno não se encontra inscrito a essa disciplina");
            return false;
        }
    }
    /**
     * Permite listar exames do aluno.
     * Imprime exame de acordo com o formato do exame.
     * @see #Exame toString() de exame para mais informação
     */
    public void listar_exames(){
        if(this.exames.isEmpty())
        {
            System.out.println("O aluno não está inscrito em nenhum exame");
        }
        for (Exame exame : this.exames) {
            System.out.print(exame);
        }
    }
    int listar_disciplinas()
    {
        int i;
        if(this.disciplinas.isEmpty())
        {
            System.out.println("O aluno não está inscrito em nenhum disciplina");
        }
        for (i=0; i<this.disciplinas.size(); i++) {
            System.out.println(i+1+" - "+this.disciplinas.get(i)+";");
        }
        return i;
    }
    /**
     * Permite obter uma nota para um determinado exame.
     * @param e (Exame) Exame que pretende saber a nota.
     * @return (double) Nota do exame.
     */
    public double get_nota(Exame e)
    {
        int index = this.exames.indexOf(e);
        return this.notas.get(index).get_nota();
    }
    /**
     * Permite saber se aluno tem estatuto especial.
     * @return boolean
     */
    public boolean especial()
    {
        return this.especial;
    }
    /**
     * Permite imprimir a informação do aluno formatada.
     * @return String formatada
     */
    @Override
    public String toString()
    {
        return String.format("Nome do aluno: %s\nNumero do aluno: %d\n"+
                "ano_matricula %d\n" +
                "Curso: %s\n" +
                "Regime: %s\n" +
                "especial: %b\n", this.nome,this.num_aluno, this.ano_matricula, this.curso.nome, this.regime, this.especial );
    }
}
