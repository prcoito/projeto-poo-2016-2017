package projeto;

/**
 * Contem informação de um NaoDocente. 
 * Esta informação é composta por uma area de investigação(String) e pela informação da classe Funcionario.
 * O NaoDocente deriva de um funcionario, pelo que herda todos os seus metodos e variaveis.
 * @author João Clara & Paulo Coito;
 */
public class NaoDocente extends Funcionario{
    String cargo;
    /**
     * Inicalizador do Docente.
     * @param nome (String) Nome do Docente.
     * @param email (String) Email do Docente.
     * @param num (int) Numero mecanográfico do Docente.
     * @param cat (String) Categoria do Docente.
     * @param cargo (String) Cargo do docente.
     * Atribui os parametros de entrada às respetivas variáveis. Inicializa todos os ArrayList's.
     */
    public NaoDocente(String nome, String email, int num, String cat, String cargo)
    {
        super(nome, email, num, cargo);
        this.cargo = cargo;
    }
}
