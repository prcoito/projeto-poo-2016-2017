package projeto;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe do projeto.
 * @author João Clara & PauloCoito
 */
public class Projeto {
    
    /**
     * Permite ler uma data inserida pelo utilizador.
     * Verifica-se a data é válida.
     * Caso a função envia ao utilizador algum atributo da data com zero, significa que houve o utilizador introduziu uma data errada pelo que esta tem de ser ignorada.
     * @return Data lida.
     */
    public static Data le_data()
    {
        Data data = new Data(0, 0, 0, 0);
        boolean invalido = true;
        Scanner sc = new Scanner(System.in);
        //Data data, double durac
        
        while(invalido)
        {
           System.out.println("Insira o dia do exame: ");
           try{
                data.dia = sc.nextInt();
            } catch( InputMismatchException e){
                System.out.println("ERRO!\nInseriu um dia invalido");
                return data;
            }
            if(data.dia<1 || data.dia>31)
            {
                System.out.println("Dia invalido\n");
            }
            else
                invalido = false;
        }
        invalido = true;
       
        while(invalido)
        {
           System.out.println("Insira o mes do exame: ");
           try{
                data.mes = sc.nextInt();
            } catch( InputMismatchException e){
                System.out.println("ERRO!\nInseriu um dia invalido");
                return data;
            }
            if(data.mes<1 || data.mes>12)
            {
                System.out.println("Mes invalido\n");
            }
            else
                invalido = false;
        }
        invalido = true;
       
        while(invalido)
        {
           System.out.println("Insira o ano do exame: ");
           try{
                data.ano = sc.nextInt();
            } catch( InputMismatchException e){
                System.out.println("ERRO!\nInseriu um dia invalido");
                return data;
            }
            if(data.ano<2016)
            {
                System.out.println("Ano invalido\n");
            }
            else
                invalido = false;
        }
        invalido = true;
       
        while(invalido)
        {
           System.out.println("Insira o hora do exame: ");
           try{
                data.hora = sc.nextInt();
            } catch( InputMismatchException e){
                System.out.println("ERRO!\nInseriu uma hora invalida (Ex: 1030 -> 10:30");
                return data;
            }
            if( data.hora <830 || data.hora>1800)
            {
                System.out.println("Hora invalido\nApenas é permitidos o inicio do exame entre as 8:30 && as 18:00");
            }
            else
                invalido = false;
        }
        
        return data;
    }
    /**
     * Lê opcao do utilizador
     * Lê uma opção entre 1 e i;
     * @param i (int) Maxima opção válida.
     * @return (int) opcao introduzida pelo utilizador
     */
    public static int le_opcao(int i)
    {
        int numero;
        Scanner sc = new Scanner(System.in);
        System.out.printf("Opcao: ");
        try{
            numero = sc.nextInt();
            while(numero>i || numero<1)
            {
                System.out.printf("Opcao invalida\nNova opcao: ");
                numero = sc.nextInt();
            }
            return numero-1;
        } catch( InputMismatchException e){
            System.out.println("ERRO!\nInseriu uma opcao invalida");
        }
        return -1;
    }
    /**
     * Lê opcao do utilizador
     * Lê uma opção entre inicio e fim;
     * @param inicio (int) Minima opção válida.
     * @param fim (int) Máxima opção válida.
     * @return (int) opcao introduzida pelo utilizador
     */
    public static int le_opcao(int inicio, int fim){
        int numero;
        Scanner sc = new Scanner(System.in);
        System.out.printf("Opcao: ");
        try{
            numero = sc.nextInt();
            while(numero>fim || numero<inicio)
            {
                System.out.printf("Opcao invalida\nNova opcao: ");
                numero = sc.nextInt();
            }
            return numero-1;
        } catch( InputMismatchException e){
            System.out.println("ERRO!\nInseriu uma opcao invalida");
        }
        return -1;
    }
    /**
     * Permite criar um exame.
     * É pedido ao utilizador que escolha de que curso e disciplina quer criar o exame, pedindo-lhe de seguida os dados para a criação do mesmo.
     * @param departamento (Departamento) Departamento que contem toda a informação dos cursos, que por sua vez tem toda a informação das disciplinas.
     * @return (Departamento) departamento com alterações guardadas.
     */
    public static Departamento Criar_exame(Departamento departamento){
        int total = departamento.imprime_cursos();
        if(total == 0)
            return departamento;
        int num_curso = le_opcao(total);
        if(num_curso == -1 )
        {
            System.out.println("ERRO!\nNao foi possivel criar exame.");
            return departamento;
        }
        Curso c = departamento.cursos.get(num_curso);
        total = c.imprime_disciplinas();
        if(total == 0)
            return departamento;
        int num_disci = le_opcao(total);
        if(num_disci == -1)
        {
            System.out.println("ERRO!\nNao foi possivel criar exame.");
            return departamento;
        }
        Disciplina d = c.disciplinas.get(num_disci);
        
        System.out.printf("Que tipo de exame pretende adicionar?\n1-Normal\n2-Recurso\n3-Especial\n");
        Scanner sc = new Scanner(System.in);
        int op = le_opcao(4);
        if(op>=0 && op<3)//porque le_opcao() retorna opcao escolhida -1 (ex. se inseriu 1 devolve 0)
        {
            char letra;
            if(op == 0)
                letra = 'N';
            else if(op == 1)
                letra = 'R';
            else
                letra = 'E';
            
            System.out.println("Insira a duracao do exame: ");
            double durac = sc.nextDouble();
            Data data = le_data();
            if(data.dia == 0 || data.mes == 0 || data.ano == 0 || data.hora == 0)   // a funcao lê data nao tinha chegado ao
            {                                                                       // fim logo nao é possivel criar exame.
                System.out.println("ERRO!\nNao foi possivel criar exame.");
                return departamento;
            }
            for (Exame exame : d.exames) {
                if (exame.data.dia == data.dia && exame.data.mes == data.mes && exame.data.ano == data.ano) {
                    System.out.println("Já existe um exame marcado desta disciplina nesse dia");
                    return departamento;
                }
            }
            for (Exame exame : d.exames) {
                if(exame.tipo().charAt(0) == letra){
                    System.out.println("Já existe um exame desse tipo marcado");
                    return departamento;
                }
            }
            if(op ==0)                                      // Docente resp
                d.exames.add(new Ex_Normal(d, data, durac, d.Docente.get(0)));
            else if(op == 1)                                // Docente resp
                d.exames.add(new Ex_Recurso(d, data, durac, d.Docente.get(0)));
            else                                                // Docente resp
                d.exames.add(new Ex_Ep_Especial(d, data, durac, d.Docente.get(0)));
            
            c.disciplinas.remove(num_disci);
            c.disciplinas.add(num_disci, d);
            departamento.cursos.remove(num_curso);
            departamento.cursos.add(num_curso, c);
            System.out.println("Exame criado com sucesso.");
            return departamento;
        }
        else
        {
            System.out.println("Erro!\n Inseriu uma opcao invalida.");
            return departamento;
        }
        
    }
    /**
     * Permite configurar salas de um exame.
     * É pedido ao utilizador que escolha de que curso e disciplina é o exame, pedindo-lhe de seguida os dados da sala.
     * É verificado se a sala escolhida está disponivel à hora do exame.
     * Se estiver adiciona a sala ao ArrayList de Salas do exame.
     * @param d (Departamento) Departamento que contem toda a informação dos cursos, que por sua vez tem toda a informação das disciplinas, que contem a informação de todos os seus exames.
     * @return (Departamento) departamento com alterações guardadas.
     * @throws java.io.IOException
     */
    public static Departamento Configurar_salas(Departamento d) throws IOException
    {
        int total = d.imprime_cursos();
        if(total == 0)
            return d;
        int op_curso = le_opcao(total);
        if(op_curso == -1)
        {
            System.out.println("ERRO!\nNao foi possivel configurar salas.");
            return d;
        }
        Curso c = d.cursos.get(op_curso);
        total = c.imprime_disciplinas();
        if(total == 0)
            return d;
        int op_disciplina = le_opcao(total);
        if(op_disciplina == -1)
        {
            System.out.println("ERRO!\nNao foi possivel configurar salas.");
            return d;
        }
        Disciplina disc = c.disciplinas.get(op_disciplina);
        total = disc.imprime_exames();
        if(total == 0)
            return d;
        int op_exame = le_opcao(total);
        if(op_exame == -1)
        {
            System.out.println("ERRO!\nNao foi possivel configurar salas.");
            return d;
        }
        Exame e = disc.exames.get(op_exame);
        
        System.out.println("Quer adicionar ou remover uma sala?\n1-Adicionar\n2-Remover");
        int op = le_opcao(2);
        
        if(op == 0)
        {
            total = d.imprime_salas();
            if(total == 0)
                return d;
            int op_sala = le_opcao(total);
            Sala s = d.salas.get(op_sala);
            if(s.verifica_disponibilidade(e))
            {
                d.salas.get(op_sala).exames.add(e);// add exame à sala.
                e.add_sala(d.salas.get(op_sala));// add sala ao exame.
                System.out.println("Sala adicionada com sucesso");
            }
            else
                System.out.println("Sala indisponivel\n");
        }
        else
        {
            if(e.salas.isEmpty())
            {
                System.out.println("O exame não tem nenhuma sala.");
                return d;
            }
            int i;
            for( i = 0; i<e.salas.size(); i++)          // imprimir salas exame
                System.out.println(i+1+" - "+e.salas.get(i).nome+";");
            
            int op_sala = le_opcao(i);
            Sala s = e.salas.get(op_sala);
            s.exames.remove(e);
            e.salas.remove(s);
            System.out.println("Sala removida do exame com sucesso");
        }
        disc.exames.remove(op_exame);
        disc.exames.add(op_exame, e);
        c.disciplinas.remove(op_disciplina);
        c.disciplinas.add(op_disciplina, disc);
        d.cursos.remove(op_curso);
        d.cursos.add(op_curso, c);
        
        return d;
    }
    /**
     * Permite listar os exames de uma disciplina.
     * É pedido ao utilizador que escolha o curso e a disciplina.
     * De seguida é-lhe apresentada a informação detalhada dos exames da disciplina.
     * @param d (Departemento) departamento que contem todos os curso, que por sua vez contêm todas as suas disciplinas.
     */
    public static void lista_exames(Departamento d)
    {
        int total = d.imprime_cursos();
        if(total == 0)
            return;
        
        int op = le_opcao(total);
        if(op == -1)
        {
            System.out.println("ERRO!\nNao foi possivel listar exames.");
            return;
        }
        Curso c = d.cursos.get(op);
        for (Disciplina disciplina : c.disciplinas) {
            System.out.println("Disciplina: " + disciplina.nome);
            if (disciplina.exames.isEmpty()) {
                System.out.println("\tNao exitem exames para esta disciplina.");
            }
            for (Exame exame : disciplina.exames) {
                System.out.println("\tEpoca: " + exame.tipo());
                System.out.print("\tData: " + exame.data);
                System.out.println("\tDuracao: " + exame.duracao);
                System.out.println("\tLista de salas:");
                for (int k = 0; k < exame.salas.size(); k++) {
                    System.out.println("\t\tSala "+k+1+": " + exame.salas.get(k).nome);
                }
                System.out.println("\tNumero de vigilantes convocados: " + exame.docentes.size());
                System.out.println("\tNumero de alunos inscritos: " + exame.alunos.size());
                System.out.println();
            }
        }
    }
    /**
     * Permite listar os exames de um aluno.
     * É pedido ao utilizador que escolha o curso e o aluno.
     * De seguida é-lhe apresentada a dos exames do aluno.
     * @param departamento (Departemento) departamento que contem todos os curso, que por sua vez contêm todas os seus alunos.
     */
    public static void listar_exames_de_aluno(Departamento departamento)
    {
        int total = departamento.imprime_cursos();
        if(total == 0)
            return;
        
        int op = le_opcao(total);
        if(op == -1)
        {
            System.out.println("ERRO!\nNao foi possivel listar exames.");
            return;
        }
        
        int total_alunos = departamento.cursos.get(op).lista_alunos();
        if(total_alunos == 0)
            return;
        
        int num_aluno = le_opcao(total_alunos);
        if(op == -1)
        {
            System.out.println("ERRO!\nNao foi possivel listar alunos.");
            return;
        }
        Aluno a = departamento.cursos.get(op).alunos.get(num_aluno);
        // listar
        a.listar_exames();
    }
    /**
     * Permite lançar as notas de um aluno.
     * Depois de escolhido o exame, é pedido ao utilizador que insira a letra do aluno que pretende lançar a nota.
     * @param d (Departemento) departamento que contem todos a informação.
     * @return (Departemento) departamento com as alterações guardadas
     */
    public static Departamento lançar_notas(Departamento d)
    {
        int total = d.imprime_cursos();
        if(total == 0)
            return d;
        
        int num_curso = le_opcao(total);
        if(num_curso == -1 )
        {
            System.out.println("ERRO!\nNao foi possivel listar.");
            return d;
        }
        Curso c = d.cursos.get(num_curso);
        
        total = c.imprime_disciplinas();
        if(total == 0)
            return d;
        
        int num_disci = le_opcao(total);
        if(num_disci == -1)
        {
            System.out.println("ERRO!\nNao foi possivel listar.");
            return d;
        }
        Disciplina disci = c.disciplinas.get(num_disci);
        
        total = disci.imprime_exames();
        if(total == 0)
            return d;
        
        int num_ex = le_opcao(total);
        if(num_ex == -1)
        {
            System.out.println("ERRO!\nNao foi possivel listar.");
            return d;
        }
        Exame ex = disci.exames.get(num_ex);   
        
        Scanner sc = new Scanner(System.in);
        System.out.print("Letra: ");
        char letra=sc.next().charAt(0);//obtém 1º caracter
        int i,num_aluno,inicio=-1;
        if(ex.alunos.isEmpty())
        {
            System.out.println("Nao ha alunos inscritos no exame");
            return d;
        }
        for (i = 0; i<ex.alunos.size(); i++) {
                if(ex.alunos.get(i).nome.charAt(0)==letra){
                    if(inicio==-1)
                        inicio=i+1;
                    System.out.println(i+1+" - "+ex.alunos.get(i).getName()+";");
                }
                if(ex.alunos.get(i).nome.charAt(0)>letra){
                    break;
                }
        }
        if(inicio==-1)  // significa que nao encontrou nenhum aluno comecado por letra
        {
            System.out.println("Nao ha aluno comecados por essa letra.");
            return d;
        }
        num_aluno = le_opcao(inicio, i);
        double valor_nota;
        System.out.print("Nota: ");
        try{
            valor_nota=sc.nextDouble();
        } catch( InputMismatchException e){
                System.out.println("ERRO!\nInseriu uma nota invalida");
                return d;
        }
        Nota nota=new Nota(valor_nota);
        Aluno al=ex.alunos.get(num_aluno);
        
        int index_exame = al.exames.indexOf(ex);
        al.notas.remove(index_exame);
        al.notas.add(index_exame,nota);
        
        ex.alunos.remove(num_aluno);
        ex.alunos.add(num_aluno, al);
        disci.exames.remove(num_ex);
        disci.exames.add(num_ex, ex);
        c.disciplinas.remove(num_disci);
        c.disciplinas.add(num_disci, disci);
        d.cursos.remove(num_curso);
        d.cursos.add(num_curso, c);
        System.out.println("Nota lancada com sucesso.");
        return d;
    }
    /**
     * Pemite listar todos os docentes convocados para a vigilancia de um exame.
     * Apenas é imprimido o nome do docente.
     * @param d (Departamento) departamento que contem toda a informação.
     */
    public static void lista_docentes_exame(Departamento d)// FUNÇAO 9
    {
        int total = d.imprime_cursos();
        if(total == 0)
            return;
        
        int num_curso = le_opcao(total);
        if(num_curso == -1 )
        {
            System.out.println("ERRO!\nNao foi possivel listar.");
            return;
        }
        Curso c = d.cursos.get(num_curso);
        
        total = c.imprime_disciplinas();
        if(total == 0)
            return;
        
        int num_disci = le_opcao(total);
        if(num_disci == -1)
        {
            System.out.println("ERRO!\nNao foi possivel listar.");
            return;
        }
        Disciplina disci = c.disciplinas.get(num_disci);
        
        total = disci.imprime_exames();
        if(total == 0)
            return;
        
        int num_ex = le_opcao(total);
        if(num_ex == -1)
        {
            System.out.println("ERRO!\nNao foi possivel listar.");
            return;
        }
        Exame ex = disci.exames.get(num_ex);   
        
        ex.listar_docentes();
        ex.listar_naodocentes();
    }
     /**
     * Pemite inscrever um aluno em exame.
     * Depois de escolhido o aluno, é pedido ao utilizador que escolha o exame para o qual o aluno se vai inscrever. 
     * @param d (Departamento) departamento que contem toda a informação.
     * @return (Departamento) departamento que contem toda a informação atualizada.
     */
    public static Departamento Increver_aluno_exame(Departamento d)
    {
        int total = d.imprime_cursos();
        if(total == 0)
            return d;
        
        int op_curso = le_opcao(total);
        if(op_curso == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse curso.");
            return d;
        }
        Curso c = d.cursos.get(op_curso);
        total = c.lista_alunos();
        if(total == 0)
            return d;
        
        int op_aluno = le_opcao(total);
        if(op_aluno == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse aluno");
            return d;
        }
        Aluno aluno = c.alunos.get(op_aluno);
        total = aluno.listar_disciplinas();
        if(total == 0)
        {
            return d;
        }
        int op_disciplina = le_opcao(total);
        if(op_disciplina == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter essa disciplina.");
            return d;
        }                                   // disciplina que aluno escolheu
        op_disciplina = c.disciplinas.indexOf(aluno.disciplinas.get(op_disciplina));
        Disciplina disc = c.disciplinas.get(op_disciplina);
        
        total = disc.imprime_exames();
        if(total == 0)   // nao existem exames logo nao pode adicionar aluno a exame dessa disciplina
            return d;
        
        int op_exame = le_opcao(total);
        if(op_exame == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse exame.");
            return d;
        }
        Exame e = disc.exames.get(op_exame);
        if(aluno.add_exame(e))
        {   // atualizar info
            e.inscrever_aluno(aluno);
            disc.exames.remove(op_exame);
            disc.exames.add(op_exame, e);
            c.disciplinas.remove(op_disciplina);
            c.disciplinas.add(op_disciplina, disc);
            d.cursos.remove(op_curso);
            d.cursos.add(op_curso, c);
            System.out.println("O aluno foi inscrito no exame com sucesso");
            return d;
        }
        else
        {
            System.out.println("Nao foi possivel inscreve-lo em exame");
        }
        return d;
    }
    
    /**
     * Permite convocar vigilantes ou funcionarios de apoio a um exame.
     * Caso seja um docente a ser adicionado à vigilância de um exame, é verificado se esse docente está disponivel para a vigilancia. Se estiver é adicionado ao ArrayList de Docentes.
     * Caso seja um nao docente é adicionado ao ArrayList de NaoDocentes
     * @param d (Departamento) departamento que contem toda a informação.
     * @return (Departamento) departamento que contem toda a informação atualizada.
     */
    public static Departamento Convocar_vigilantes_e_funcionarios(Departamento d)
    {
        int total = d.imprime_cursos();
        if(total == 0)
            return d;
        
        int op_curso = le_opcao(total);
        if(op_curso == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse curso.");
            return d;
        }
        Curso c = d.cursos.get(op_curso);
        total = c.imprime_disciplinas();
        if(total == 0)
            return d;
        
        int op_disciplina = le_opcao(total);
        if(op_disciplina == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter essa disciplina.");
            return d;
        }
        Disciplina disc = c.disciplinas.get(op_disciplina);
        total = disc.imprime_exames();
        if(total == 0)   // nao existem exames logo nao pode adicionar aluno a exame dessa disciplina
            return d;
        
        int op_exame = le_opcao(total);
        if(op_exame == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse exame.");
            return d;
        }
        Exame e = disc.exames.get(op_exame);
        System.out.println("Que tipo que Funcionario que adicionar?\n"
                            + "1- Nao Docente;\n"
                            + "2- Docente");
        int op = le_opcao(2);// é naoDocente ou Docente
        if(op == 0)// se naoDocente add
        {
            total = d.imprime_nao_docentes();
            if(total == 0)
                return d;
            
            int op_nd = le_opcao(total);
            NaoDocente nd = d.nao_docente.get(op_nd);
            int existe = e.naodocentes.indexOf(nd);
            if(existe == -1)
            {
                e.add_nao_docente(nd);
                nd.exame.add(e);
                System.out.println("O nao docente convocado ao apoio do exame com sucesso.");
            }
            else
                System.out.println("O nao docente ja se encontra convocado.");
        }
        else // se Docente verifica disponibilidade
        {
            total = d.imprime_docentes();
            if(total == 0)
                return d;
            
            int op_d = le_opcao(total);
            Docente doc = d.docente.get(op_d);
            if(doc.verifica_disponibilidade(e))
            {
                e.add_vigilante(doc);
                doc.exame.add(e);
                System.out.println("O docente adicionado à vigilancia do exame com sucesso.");
            }
            else
            {
                System.out.println("O docente nao pode vigiar esse exame porque tem sobreposiçao.");
                return d;
            }
        }
        disc.exames.remove(op_exame);
        disc.exames.add(op_exame, e);
        c.disciplinas.remove(op_disciplina);
        c.disciplinas.add(op_disciplina, disc);
        d.cursos.remove(op_curso);
        d.cursos.add(op_curso, c);
        return d;
    }
    /**
     * Lista docentes e não docentes convocados para um exame.
     * Apenas é imprimido o nome do docente/não docente.
     * @param d (Departamento) departamento que contem toda a informação necessária.
     */
    public static void Listar_docentes_e_funcionarios_de_um_exame(Departamento d)
    {
        int total = d.imprime_cursos();
        if(total == 0)
            return;
        
        int op_curso = le_opcao(total);
        if(op_curso == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse curso.");
            return;
        }
        Curso c = d.cursos.get(op_curso);
        total = c.imprime_disciplinas();
        if(total == 0)
            return;
        
        int op_disciplina = le_opcao(total);
        if(op_disciplina == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter essa disciplina.");
            return;
        }
        Disciplina disc = c.disciplinas.get(op_disciplina);
        total = disc.imprime_exames();
        if(total == 0)   // nao existem exames logo nao pode adicionar aluno a exame dessa disciplina
            return;
        
        int op_exame = le_opcao(total);
        if(op_exame == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse exame.");
            return;
        }
        Exame e = disc.exames.get(op_exame);
        
        e.listar_naodocentes();
        e.listar_docentes();
        
    }
    /**
     * Pemite listar os exames de um docente ou não docente.
     * Pede-se qual das opcoes o utilizador pretende listar, e de seguida são listados todos os exames do funcionario escolhido.
     * @param d (Departamento) departamento que contem toda a informação necessária.
     */
    public static void Listar_exames_de_um_docente_ou_funcionario(Departamento d)
    {
        System.out.println("Que tipo que Funcionario quer imprimir exames?\n"
                            + "1- Nao Docente;\n"
                            + "2- Docente");
        int op = le_opcao(2);// é naoDocente ou Docente
        if(op == 0)// se naoDocente add
        {
            int total = d.imprime_nao_docentes();
            if(total == 0)
                return;
            
            int op_nd = le_opcao(total);
            d.nao_docente.get(op_nd).listar_exames();
        }
        else
        {
            int total = d.imprime_docentes();
            if(total == 0)
                return;
            
            int op_d = le_opcao(total);
            d.docente.get(op_d).listar_exames();
        }
    }
    
    /**
     * Permite listar os alunos e respetivas classificaçoes num exame.
     * Depois de escolhido o exame, é listado o nome e a nota do aluno.
     * @param d (Departamento) departamento que contem toda a informação.
     */
    public static void Listar_alunos_e_classificacoes(Departamento d)
    {
        int total = d.imprime_cursos();
        if(total == 0)
            return;
        
        int op_curso = le_opcao(total);
        if(op_curso == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse curso.");
            return;
        }
        Curso c = d.cursos.get(op_curso);
        total = c.imprime_disciplinas();
        if(total == 0)
            return;
        
        int op_disciplina = le_opcao(total);
        if(op_disciplina == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter essa disciplina.");
            return;
        }
        Disciplina disc = c.disciplinas.get(op_disciplina);
        total = disc.imprime_exames();
        if(total == 0)   // nao existem exames logo nao pode adicionar aluno a exame dessa disciplina
            return;
        int op_exame = le_opcao(total);
        if(op_exame == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse exame.");
            return;
        }
        Exame e = disc.exames.get(op_exame);
        e.listar_pauta();
        
    }
    /**
     * Permite listar as classificaçoes de um exame.
     * Apenas são listadas as classificações.
     * @param d (Departamento) departamento que contem toda a informação.
     */
    public static void Listar_notas(Departamento d){
        int total = d.imprime_cursos();
        if(total == 0)
            return;
        
        int op_curso = le_opcao(total);
        if(op_curso == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse curso.");
            return;
        }
        Curso c = d.cursos.get(op_curso);
        total = c.imprime_disciplinas();
        if(total == 0)
            return;
        
        int op_disciplina = le_opcao(total);
        if(op_disciplina == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter essa disciplina.");
            return;
        }
        Disciplina disc = c.disciplinas.get(op_disciplina);
        total = disc.imprime_exames();
        if(total == 0)   // nao existem exames logo nao pode adicionar aluno a exame dessa disciplina
            return;
        
        int op_exame = le_opcao(total);
        if(op_exame == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse exame.");
            return;
        }
        Exame e = disc.exames.get(op_exame);
        
        e.listar_notas();
    }
    /**
     * Permite remover um exame de uma disciplina.
     * @param d (Departamento) departamento que contem toda a informação.
     * @return (Departamento) departamento que contem toda a informação atualizada.
     */
    public static Departamento remove_exame(Departamento d)
    {
        int total = d.imprime_cursos();
        if(total == 0)
            return d;
        
        int op_curso = le_opcao(total);
        if(op_curso == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse curso.");
            return d;
        }
        Curso c = d.cursos.get(op_curso);
        total = c.imprime_disciplinas();
        if(total == 0)
            return d;
        
        int op_disciplina = le_opcao(total);
        if(op_disciplina == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter essa disciplina.");
            return d;
        }
        Disciplina disc = c.disciplinas.get(op_disciplina);
        total = disc.imprime_exames();
        if(total == 0)  
            return d;
        
        int op_exame = le_opcao(total);
        if(op_exame == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse exame.");
            return d;
        }
        Exame e = disc.exames.get(op_exame);
        if(!e.alunos.isEmpty())
        {
            for (Aluno aluno : e.alunos) {
                aluno.exames.remove(e);// remove os exames aos alunos
            }
        }
        disc.exames.remove(op_exame);
        c.disciplinas.remove(op_disciplina);
        c.disciplinas.add(op_disciplina, disc);
        d.cursos.remove(op_curso);
        d.cursos.add(op_curso, c);
        System.out.println("Exame removido com sucesso.");
        return d;
    }
    /**
     * Permite remover vigilante de um exame.
     * @param d (Departamento) departamento que contem toda a informação.
     * @return (Departamento) departamento que contem toda a informação atualizada.
     */
    public static Departamento remove_vigilantes(Departamento d)
    {
        int total = d.imprime_cursos();
        if(total == 0)
            return d;
        
        int op_curso = le_opcao(total);
        if(op_curso == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse curso.");
            return d;
        }
        Curso c = d.cursos.get(op_curso);
        total = c.imprime_disciplinas();
        if(total == 0)
            return d;
        
        int op_disciplina = le_opcao(total);
        if(op_disciplina == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter essa disciplina.");
            return d;
        }
        Disciplina disc = c.disciplinas.get(op_disciplina);
        total = disc.imprime_exames();
        if(total == 0)   // nao existem exames logo nao pode adicionar aluno a exame dessa disciplina
            return d;
        int op_exame = le_opcao(total);
        if(op_exame == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse exame.");
            return d;
        }
        Exame e = disc.exames.get(op_exame);
        System.out.println("Que tipo que Funcionario quer imprimir exames?\n"
                            + "1- Nao Docente;\n"
                            + "2- Docente");
        int op = le_opcao(2);// é naoDocente ou Docente
        if(op == 0 && !(e.docentes.isEmpty()))// se naoDocente
        {
            total = e.listar_naodocentes();
            int op_nd = le_opcao(total);
            e.naodocentes.get(op_nd).exame.remove(e);
            e.naodocentes.remove(op_nd);
            System.out.println("Vigilante removido do exame com sucesso.");
        }
        else if( op == 1 && !(e.docentes.isEmpty()))
        {
            total = e.listar_docentes();
            int op_nd = le_opcao(total);
            e.docentes.get(op_nd).exame.remove(e);
            e.docentes.remove(op_nd);
            System.out.println("Vigilante removido do exame com sucesso.");
        }
        else
        {
            if(op == 0)
                System.out.println("Nao esta associado nenhum docente");
            else
                 System.out.println("Nao esta associado nenhum docente");
        }
        // atualiza info
        disc.exames.remove(op_exame);
        disc.exames.add(e);
        c.disciplinas.remove(op_disciplina);
        c.disciplinas.add(op_disciplina, disc);
        d.cursos.remove(op_curso);
        d.cursos.add(op_curso, c);
        
        return d;
    }
    /**
     * Permite remover aluno de um exame.
     * @param d (Departamento) departamento que contem toda a informação.
     * @return (Departamento) departamento que contem toda a informação atualizada.
     */
    public static Departamento remove_aluno_exame(Departamento d)
    {
        int total = d.imprime_cursos();
        if(total == 0)
            return d;
        
        int op_curso = le_opcao(total);
        if(op_curso == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse curso.");
            return d;
        }
        Curso c = d.cursos.get(op_curso);
        total = c.imprime_disciplinas();
        if(total == 0)
            return d;
        
        int op_disciplina = le_opcao(total);
        if(op_disciplina == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter essa disciplina.");
            return d;
        }
        Disciplina disc = c.disciplinas.get(op_disciplina);
        total = disc.imprime_exames();
        if(total == 0)   // nao existem exames logo nao pode adicionar aluno a exame dessa disciplina
            return d;
        
        int op_exame = le_opcao(total);
        if(op_exame == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse exame.");
            return d;
        }
        Exame e = disc.exames.get(op_exame);
        
        total = e.listar_alunos();
        if(total == 0)
            return d;
        
        int op_aluno = le_opcao(total);
        
        e.alunos.get(op_aluno).exames.remove(e); // remove exame aluno
        e.alunos.remove(op_aluno);  // remove aluno exame
        // atualiza info
        disc.exames.remove(op_exame);
        disc.exames.add(e);
        c.disciplinas.remove(op_disciplina);
        c.disciplinas.add(op_disciplina, disc);
        d.cursos.remove(op_curso);
        d.cursos.add(op_curso, c);
        System.out.println("Aluno removido do exame com sucesso.");
        return d;
    }
    /**
     * Permite adicionar um aluno a um curso.
     * @param d (Departamento) departamento que contem toda a informação.
     * @return (Departamento) departamento que contem toda a informação atualizada.
     */
    public static Departamento adiciona_aluno_curso(Departamento d)
    {
        int total = d.imprime_cursos();
        if(total == 0)
            return d;
        
        int op_curso = le_opcao(total);
        if(op_curso == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse curso.");
            return d;
        }
        Curso c = d.cursos.get(op_curso);
        int num_aluno = c.alunos.size() +1;
        int ano = 2016;
        
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Insira o nome do aluno: ");
        String nome = sc.nextLine();
        String email = nome.concat("@"+d.nome+".mail.pt");
        System.out.print("Insira o regime do aluno: ");
        String regime = sc.next();
        regime = regime.toLowerCase();
        while(regime.compareTo("normal") != 0 && regime.compareTo("especial") != 0)
        {
            System.out.print("Regime invalido\nInsira o regime do aluno(insira sair para terminar)\n");
            regime = sc.next().toLowerCase();
            if(regime.equals("sair"))
                return d;
        }
        c.add_aluno(nome, email, num_aluno, ano, regime);
        
        // atualizar info
        d.cursos.remove(op_curso);
        d.cursos.add(op_curso, c);
        System.out.println("Aluno adicionado ao curso com sucesso.");
        return d;
    }
    /**
     * Permite remover um aluno de um curso.
     * @param d (Departamento) departamento que contem toda a informação.
     * @return (Departamento) departamento que contem toda a informação atualizada.
     */
    public static Departamento remove_aluno_curso(Departamento d)
    {
        int total = d.imprime_cursos();
        if(total == 0)
            return d;
        
        int op_curso = le_opcao(total);
        if(op_curso == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse curso.");
            return d;
        }
        Curso c = d.cursos.get(op_curso);
        total = c.lista_alunos();
        if(total == 0)
            return d;
        
        int op_aluno = le_opcao(total);
        Aluno a = c.alunos.get(op_aluno);
        
        for (Disciplina disciplina : c.disciplinas) {
            disciplina.Aluno.remove(a); // remover o aluno das disciplinas que esta inscrito
        }
        
        c.alunos.remove(op_aluno);
        d.cursos.remove(op_curso);
        d.cursos.add(op_curso, c);
        System.out.println("Aluno removido do curso com sucesso.");
        return d;
    }
    /**
     * Permite adicionar um aluno a uma disciplina.
     * @param d (Departamento) departamento que contem toda a informação.
     * @return (Departamento) departamento que contem toda a informação atualizada.
     */
    public static Departamento adiciona_aluno_disciplina(Departamento d)
    {
        int total = d.imprime_cursos();
        if(total == 0)
            return d;
        
        int op_curso = le_opcao(total);
        if(op_curso == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse curso.");
            return d;
        }
        Curso c = d.cursos.get(op_curso);
        total = c.lista_alunos();
        if(total == 0)
            return d;
        
        int op_aluno = le_opcao(total);
        Aluno a = d.cursos.get(op_curso).alunos.get(op_aluno);
        total = c.imprime_disciplinas();
        if(total == 0)
            return d;
        
        int op_disc = le_opcao(total);         
        Disciplina disc = c.disciplinas.get(op_disc);
        
        if(!disc.Aluno.contains(a))
        {
            disc.add_aluno(a);
            a.disciplinas.add(disc);
            System.out.println("Aluno adicionado à disciplina com sucesso.");
        }
        else{
            System.out.println("Aluno ja esta inscrito na disciplina.");
            return d;
        }
        
        // atualiza info
        c.alunos.remove(op_aluno);
        c.alunos.add(op_aluno, a);
        c.disciplinas.remove(op_disc);
        c.disciplinas.add(op_disc, disc);
        d.cursos.remove(op_curso);
        d.cursos.add(op_curso, c);
        return d;
    }
    /**
     * Permite remover um aluno de uma disciplina.
     * @param d (Departamento) departamento que contem toda a informação.
     * @return (Departamento) departamento que contem toda a informação atualizada.
     */
    public static Departamento remove_aluno_disciplina(Departamento d)
    {
        int total = d.imprime_cursos();
        if(total == 0)
            return d;
        
        int op_curso = le_opcao(total);
        if(op_curso == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse curso.");
            return d;
        }
        Curso c = d.cursos.get(op_curso);
        total = c.lista_alunos();
        if(total == 0)
            return d;
        
        int op_aluno = le_opcao(total);
        Aluno a = c.alunos.get(op_aluno);
        total = c.imprime_disciplinas();
        if(total == 0)
            return d;
        
        int op_disc = le_opcao(total);         
        if(op_disc == -1)
            return d;
        Disciplina disc = c.disciplinas.get(op_disc);
        disc.Aluno.remove(a);
        a.disciplinas.remove(disc);
        
        // atualiza info
        c.alunos.remove(op_aluno);
        c.alunos.add(op_aluno, a);
        c.disciplinas.remove(op_disc);
        c.disciplinas.add(op_disc, disc);
        d.cursos.remove(op_curso);
        d.cursos.add(op_curso, c);
        System.out.println("Aluno removido da disciplina com sucesso");
        return d;
    }
    /**
     * Permite crair um novo curso.
     * @param d (Departamento) departamento que contem toda a informação.
     * @return (Departamento) departamento que contem toda a informação atualizada.
     */
    public static Departamento cria_curso(Departamento d)
    {
        Scanner sc = new Scanner(System.in);
        System.out.print("Insira o nome do curso:");
        String nome = sc.next();
        System.out.print("Insira a duraçao do curso: ");
        int duracao = le_opcao(1,5);
        if(duracao == -1)
        {
            System.out.println("Nao foi possivel criar o curso");
            return d;
        }
        System.out.print("Insira o grau do curso: ");
        String grau = sc.next().toLowerCase();
        if(grau.compareTo("licenciatura")==0||grau.compareTo("mestrado")==0||grau.compareTo("doutoramento")==0)
        {
            d.cursos.add(new Curso(nome,duracao,grau));
            System.out.println("Curso adicionado com sucesso");
        }
        else
            System.out.println("Nao existe esse grau");
        
        return d;
    }
    /**
     * Permite remover um curso.
     * @param d (Departamento) departamento que contem toda a informação.
     * @return (Departamento) departamento que contem toda a informação atualizada.
     */
    public static Departamento remover_curso(Departamento d)
    {
        int total = d.imprime_cursos();
        if(total == 0)
            return d;
        
        int op_curso = le_opcao(total);
        if(op_curso == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse curso.");
            return d;
        }
        
        d.cursos.remove(op_curso);
        System.out.println("Curso removido com sucesso");
        return d;
    }
    /**
     * Permite criar uma disciplina.
     * @param d (Departamento) departamento que contem toda a informação.
     * @return (Departamento) departamento que contem toda a informação atualizada.
     */
    public static Departamento cria_disciplina(Departamento d)
    {
        int total = d.imprime_cursos();
        if(total == 0)
            return d;
        
        int op_curso = le_opcao(total);
        if(op_curso == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse curso.");
            return d;
        }
        Curso c = d.cursos.get(op_curso);
        
        System.out.print("Insira o nome da disciplina a criar: ");
        Scanner sc = new Scanner(System.in);
        String nome = sc.next();
        Disciplina disc = new Disciplina(nome);
        System.out.println("Insira o docente responsavel pela disciplina");
        total = d.imprime_docentes();
        if(total == 0)
            return d;
        
        int op_doc = le_opcao(total);
        disc.Docente.add(d.docente.get(op_doc));
        
        c.add_disciplina(disc);
        
        //atualizar info
        d.cursos.remove(op_curso);
        d.cursos.add(op_curso, c);
        System.out.println("Disciplina criada com sucesso");
        return d;
    }
    /**
     * Permite remover uma disciplina.
     * @param d (Departamento) departamento que contem toda a informação.
     * @return (Departamento) departamento que contem toda a informação atualizada.
     */
    public static Departamento remove_disciplina(Departamento d)
    {
        int total = d.imprime_cursos();
        if(total == 0)
            return d;
        
        int op_curso = le_opcao(total);
        if(op_curso == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter esse curso.");
            return d;
        }
        Curso c = d.cursos.get(op_curso);
        total = c.imprime_disciplinas();
        if(total == 0)
            return d;
        
        int op_disc = le_opcao(total);
        if(op_disc == -1)
        {
            System.out.println("ERRO!\nNao foi possivel obter essa disciplina.");
            return d;
        }
        Disciplina d_a_remover = c.disciplinas.get(op_disc);
        for (Aluno Aluno : d_a_remover.Aluno) {     // remove a disciplina ao aluno
            Aluno.disciplinas.remove(d_a_remover);
        }
        
        // atualizar info
        c.disciplinas.remove(op_disc);
        d.cursos.remove(op_curso);
        d.cursos.add(op_curso, c);
        System.out.println("Disciplina removida com sucesso");
        return d;
    }
    /**
     * Funcionalidades extras implementadas.
     * É pedido ao utilizador qual das opções pretende.
     * @param d (Departamento) departamento que contem toda a informação.
     * @return (Departamento) departamento que contem toda a informação atualizada.
     */
    public static Departamento Extras(Departamento d)
    {
        int opcao;
        Scanner sc = new Scanner(System.in);
        System.out.print("  ________________________________________________\n"
                       + " /                                                \\ \n"
                       + "| 1.Remover exame                                  |\n"
                       + "| 2.Remover vigilantes e funcionarios              |\n"
                       + "| 3.Remover alunos em exame                        |\n"
                       + "| 4.Adicionar aluno a curso                        |\n"
                       + "| 5.Remover aluno de curso                         |\n"
                       + "| 6.Adicionar aluno a disciplina                   |\n"
                       + "| 7.Remover aluno de disciplina                    |\n"
                       + "| 8.Criar curso                                    |\n"
                       + "| 9.Remover curso                                  |\n"
                       + "| 10.Criar disciplina                              |\n"
                       + "| 11.Remover disciplina                            |\n"
                       + " \\________________________________________________/\n"
                       + "Opcao: ");
        try{
            sc = new Scanner(System.in);
            opcao = sc.nextInt();
        }catch( InputMismatchException e){
            System.out.println("ERRO!");
            opcao = 0;
        }
        switch(opcao)
        {
            case 1: {
                        d = remove_exame(d);
                        break;
                    }
        
            case 2: { 
                        d = remove_vigilantes(d);
                        break;
                    }
        
            case 3: {
                        d = remove_aluno_exame(d); 
                        break;
                    }
        
            case 4: {
                        d = adiciona_aluno_curso(d);
                        break;
                    }
            case 5: {
                        d = remove_aluno_curso(d);
                        break;
                    }
        
            case 6: {
                        d = adiciona_aluno_disciplina(d);
                        break;
                    }
        
            case 7: {
                        d = remove_aluno_disciplina(d);
                        break;
                    } 
                
            case 8: {
                        d = cria_curso(d);
                        break;
                    }
            case 9: {
                        d = remover_curso(d);
                        break;
                    }
            case 10:{
                        d = cria_disciplina(d);
                        break;
                    }
            
            case 11:{
                        d = remove_disciplina(d);
                        break;
                    }
            default:
                    System.out.println("Opcao invalida");
            
        }
        System.out.println();
        return d;
    }
    /**
     * Funcionalidades principais implementadas.
     * É pedido ao utilizador qual das opções pretende.
     * @param departamento (Departamento) departamento que contem toda a informação.
     * @return (Departamento) departamento que contem toda a informação atualizada.
     * @throws java.io.IOException
     */
    public static Departamento menu(Departamento departamento) throws IOException{
        int numero;
        Scanner sc = new Scanner(System.in);
        do{
            System.out.print("/--------------------------------------------------\\ \n"
                           + "| 1.Criar exame                                    |\n"
                           + "| 2.Configurar salas                               |\n"
                           + "| 3.Convocar vigilantes e funcionarios             |\n"
                           + "| 4.Inscrever alunos em exame                      |\n"
                           + "| 5.Lancar notas                                   |\n"
                           + "| 6.Listar exames                                  |\n"
                           + "| 7.Listar alunos e classificacoes                 |\n"
                           + "| 8.Listar exames de aluno                         |\n"
                           + "| 9.Listar docentes e funcionarios de um exame     |\n"
                           + "| 10.Listar exames de um docente ou funcionario    |\n"
                           + "| 11.Listar notas                                  |\n"
                           + "| 12.Extras                                        |\n"
                           + "| 13.Sair                                          |\n"
                          + "\\--------------------------------------------------/\n"
                           + "Opcao: ");
                try{
                     sc = new Scanner(System.in);
                     numero = sc.nextInt();
                }catch( InputMismatchException e){
                    System.out.println("ERRO!");
                    numero = 0;
                }
                
                switch(numero){
                    case 1: departamento = Criar_exame(departamento);
                            break;
                    case 2: departamento = Configurar_salas(departamento);
                            break;
                    case 3: departamento = Convocar_vigilantes_e_funcionarios(departamento);
                            break;
                    case 4: departamento = Increver_aluno_exame(departamento);
                            break;
                    case 5: departamento = lançar_notas(departamento);
                            break;
                    case 6: lista_exames(departamento);
                            break;
                    case 7: Listar_alunos_e_classificacoes(departamento);
                            break;
                    case 8: listar_exames_de_aluno(departamento);
                            break;
                    case 9: Listar_docentes_e_funcionarios_de_um_exame(departamento);
                            break;
                    case 10: Listar_exames_de_um_docente_ou_funcionario(departamento);
                            break;
                    case 11: Listar_notas(departamento);
                            break;
                    case 12: departamento = Extras(departamento);
                    case 13: break; // 13 == SAIR
                    default:
                            System.out.println("Opcao invalida");
                }
                System.out.println();
            } while(numero != 13);
        return departamento;
    }
    
    /**
     * Função para caregar informação de um ficheiro para memoria.
     * Esta informação é guardada num ArrayList de Departamentos.
     * @param inStream (ObjectInputStream) Ficheiro a carregar.
     * @return ArrayList de Departamento.
     */
    public static ArrayList<Departamento> carregar(ObjectInputStream inStream)
    {
        ArrayList<Departamento> departamento = null;
        try{
            departamento = (ArrayList<Departamento>)inStream.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Projeto.class.getName()).log(Level.SEVERE, null, ex);
        }
        return departamento;  
    }
    /**
     * Funcao que cria uma base de dados minima para que as funcionalidades do programa possam ser testadas.
     * @param outstream (ObjectOutputStream) Ficheiro que ficará com os dados gerados guardados em disco.
     */
    public static void gerar(ObjectOutputStream outstream)
    {
        System.out.println("A gerar...");
        try{
            ArrayList<Departamento> departamentos = new ArrayList<Departamento>();
            Departamento depart1 = new Departamento("DEI");
            Curso LEI = new Curso("LEI", 3, "Licenciatura");
            Disciplina Disc1 = new Disciplina("Disc1");
            Disciplina Disc2 = new Disciplina("Disc2");
            Disciplina Disc3 = new Disciplina("Disc3");
            Disciplina Disc4 = new Disciplina("Disc4");
            LEI.disciplinas.add(Disc1);
            LEI.disciplinas.add(Disc2);
            LEI.disciplinas.add(Disc3);
            LEI.disciplinas.add(Disc4);
            Docente Do1 = new Docente("Do1", "Do1"+"@"+depart1.nome+".mail.pt", 1, "c1", "a1");
            Docente Do2 = new Docente("Do2", "Do2"+"@"+depart1.nome+".mail.pt", 2, "c1", "a1");
            Docente Do3 = new Docente("Do3", "Do3"+"@"+depart1.nome+".mail.pt", 3, "c1", "a1");
            Docente Do4 = new Docente("Do4", "Do4"+"@"+depart1.nome+".mail.pt", 4, "c1", "a1");
            Aluno Aluno1 = new Aluno("AL1", "AL1"+"@"+depart1.nome+".mail.pt", 1, LEI, 1, "normal");
            Aluno Aluno2 = new Aluno("AL2", "AL2"+"@"+depart1.nome+".mail.pt", 2, LEI, 1, "normal");
            Aluno Aluno3 = new Aluno("AL3", "AL3"+"@"+depart1.nome+".mail.pt", 3, LEI, 4, "especial");
            Aluno Aluno4 = new Aluno("AL4", "AL4"+"@"+depart1.nome+".mail.pt", 4, LEI, 2, "normal");
            Aluno1.disciplinas.add(Disc1);
            Aluno2.disciplinas.add(Disc1);
            Aluno3.disciplinas.add(Disc1);
            Aluno4.disciplinas.add(Disc1);
            Disc1.Aluno.add(Aluno1);
            Disc1.Aluno.add(Aluno2);
            Disc1.Aluno.add(Aluno3);
            Disc1.Aluno.add(Aluno4);
            Data data1 = new Data(15, 1, 2017, 1400);
            Data data2 = new Data(25, 1, 2017, 1430);
            Data data3 = new Data(20, 7, 2017, 1500);
            Disc1.Docente.add(Do1);
            Disc2.Docente.add(Do2);
            Disc3.Docente.add(Do3);
            Disc4.Docente.add(Do4);
            Disc1.exames.add(new Ex_Normal(Disc1, data1, 1.5, Do1));
            Disc1.exames.add(new Ex_Recurso(Disc1, data2, 1.5, Do1));
            Disc1.exames.add(new Ex_Ep_Especial(Disc1, data3, 2, Do1));
            LEI.alunos.add(Aluno1);
            LEI.alunos.add(Aluno2);
            LEI.alunos.add(Aluno3);
            LEI.alunos.add(Aluno4);
            depart1.cursos.add(LEI);
            departamentos.add(depart1);
            Sala sala1 = new Sala("S1", 3);
            Sala sala2 = new Sala("S2", 2);
            depart1.salas.add(sala1);
            depart1.salas.add(sala2);
            NaoDocente ND1 = new NaoDocente("ND1", "ND1"+"@"+depart1.nome+".mail.pt", 1, "cat1", "car1");
            NaoDocente ND2 = new NaoDocente("ND2", "ND2"+"@"+depart1.nome+".mail.pt", 2, "cat1", "car1");
            NaoDocente ND3 = new NaoDocente("ND3", "ND3"+"@"+depart1.nome+".mail.pt", 3, "cat1", "car1");
            NaoDocente ND4 = new NaoDocente("ND4", "ND4"+"@"+depart1.nome+".mail.pt", 4, "cat1", "car1");
            depart1.docente.add(Do1);
            depart1.docente.add(Do2);
            depart1.docente.add(Do3);
            depart1.docente.add(Do4);
            depart1.nao_docente.add(ND1);
            depart1.nao_docente.add(ND2);
            depart1.nao_docente.add(ND3);
            depart1.nao_docente.add(ND4);
            Exame temp = (Exame)Disc1.exames.get(0);
            temp.salas.add(sala1);
            Disc1.exames.remove(0);
            Disc1.exames.add(0, temp);
            outstream.writeObject(departamentos);
            
        }   catch (IOException ex) {
            Logger.getLogger(Projeto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Funcao que inicia o programa.
     * Se existir uma dase de dados a ler, esta é carregada para memoria.
     * Se não existir e criada uma base de dados minima para que se possam testar as funcionalidades do programa.
     * Depois de carregada a base de dados para memoria, o programa segue a sua normal execução.
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
        ArrayList<Departamento> departamento = null;
        try{
            FileInputStream iStream = new FileInputStream(new File("database.jcpc"));
            try (ObjectInputStream inStream = new ObjectInputStream(iStream)) {
                departamento = carregar(inStream);
                iStream.close();
            }
            
        } catch(FileNotFoundException e){
            System.out.printf("Ficheiro nao encontrado. A criar ficheiro de teste...\n");
            File file = new File("database.jcpc");
            FileOutputStream oStream = new FileOutputStream(file);
            try (ObjectOutputStream outstream = new ObjectOutputStream(oStream)) {
                gerar(outstream);
            }
            
            System.out.printf("Ficheiro de teste criado. A carregar ficheiro de teste...\n");
            try (FileInputStream iStream = new FileInputStream(new File("database.jcpc")); ObjectInputStream inStream = new ObjectInputStream(iStream)) {
                departamento = carregar(inStream);
                
            }
            System.out.printf("Ficheiro de teste carregado\n");
        } catch(IOException e){
            System.out.printf("Ocorreu uma exepcao %s\n", e);
        }
        if(departamento!=null)
        {
            
            Departamento d = (Departamento)departamento.get(0);
            
            d = menu(d);
            
            File file = new File("database.jcpc");
            FileOutputStream oStream = new FileOutputStream(file);
            try (ObjectOutputStream outstream = new ObjectOutputStream(oStream)) {
                departamento.remove(0);
                departamento.add(d);
                outstream.writeObject(departamento);
            }
        }
        else
            System.out.println("ERRO AO LER DB.");
        
    }
}
    
