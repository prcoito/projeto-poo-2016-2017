package projeto;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Contem informação de um curso. 
 * Esta informação é composta pelo nome do curso (String), o seu grau correspondente (String), a duração (int), em anos, um ArrayList de aluno e um ArrayList de Disciplinas. 
 * @author João Clara & Paulo Coito;
 */
public class Curso  implements Serializable{
    String nome, grau;
    int duracao;
    ArrayList<Aluno> alunos;
    ArrayList<Disciplina> disciplinas;
    /**
     * Inicializador do Curso
     * @param nome (String) Nome do Curso.
     * @param duracao (int) Duração do Curso.
     * @param grau (String) Grau do Curso (Licenciatura, Mestrado, Doutoramento)
     * Atribui os parametros de entrada às respetivas variáveis. Inicializa todos os ArrayList's.
     */
    public Curso(String nome, int duracao, String grau)
    {
        this.nome = nome;
        this.duracao = duracao;
        this.grau = grau;
        alunos = new ArrayList<Aluno>();
        disciplinas = new ArrayList<Disciplina>();
    }
    /**
     * Permite adicionar uma disciplina ao ArrayList de disciplinas do curso. Esta disciplina é adicionada no fim do ArrayList.
     * @param d (Disciplina) Disciplina a adicionar.
     * Dada uma String adiciona uma disciplina com esse nome ao ArrayList de Disciplinas.
     * Para mais informações acerca da disciplina consultar respetivo Javadoc.
     * @see Disciplina para mais informação acerca da Disciplina a adicionar.
     */
    public void add_disciplina(Disciplina d)
    {
        disciplinas.add(d);
    }
    /**
     * Permite adicionar um aluno ao ArrayList de alunos do curso. O aluno é posto no ArrayList por ordem alfabetica.
     * @param nome (String) Nome do aluno.
     * @param email (String) Email do aluno.
     * @param num_aluno (int) Numero do aluno.
     * @param ano (int) Ano em que o aluno começou a frequentar o curso.
     * @param regime (String) Normal, Especial(Trabalhador-Estudante).
     * Dado a informação do aluno permite que este seja adicionado ao curso respetivo.
     * @see Aluno para mais informação acerca do Aluno a adicionar.
     */
    public void add_aluno(String nome, String email, int num_aluno, int ano, String regime)
    {
        for (int i=0; i<alunos.size();i++)   // add aluno por ordem alfabetica
        {
            if(nome.compareTo(alunos.get(i).nome)<0)
            {
                this.alunos.add(i, new Aluno(nome, email, num_aluno, this, ano, regime));
                return;
            }
        }
        alunos.add(new Aluno(nome, email, num_aluno, this, ano, regime));
    }
    /**
     * Permite listar todos os exames do curso.
     * O metodo percorre o ArrayList de disciplinas e, para cada disciplina, percorre o ArrayList de exames, imprimindo-os.
     */
    public void listar_exames()
    {
       
       for(int i = 0; i< this.disciplinas.size() ; i++)
       {
           Disciplina d = this.disciplinas.get(i);
           for(int j = 0; j<d.exames.size(); j++)
           {
               System.out.print(d.exames.get(i));
           }
        }
    }
    /**
     * Permite imprimir todas as disciplinas do curso.
     * É percorrido o ArrayList de disciplinas e é impresso o seu nome.
     * @return numero total de disciplinas do curso.
     */
    public int imprime_disciplinas()
    {
        int i;
        if(this.disciplinas.isEmpty())
        {
            System.out.println("Nao existem disciplinas neste curso");
            return 0;
        }
        System.out.println("Lista de disciplinas:");
        for (i = 0; i<this.disciplinas.size(); i++) {
            System.out.println(i+1+" - "+this.disciplinas.get(i).nome+";");
        }
        return i;
    }
    /**
     * Permite imprimir todos os alunos de um curso.
     * Para cada aluno é impresso o seu nome e respectivo número de aluno.
     * @return o numero total de alunos do curso
     */
    public int lista_alunos() {
        int i;
        
        if(this.alunos.isEmpty())
        {
            System.out.println("Nao existem alunos inscritos neste curso");
            return 0;
        }
        for (i = 0; i<this.alunos.size(); i++) {
            System.out.println(i+1+" - "+this.alunos.get(i).nome+" "+this.alunos.get(i).num_aluno+";");
        }
        return i;
    }
}