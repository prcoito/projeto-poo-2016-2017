
package projeto;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Contem informação de um departamento. 
 * Esta informação é composta pelo nome do Departamento, um ArrayList de Cursos, um ArrayList de Salas, um ArrayList de Docente e um ArrayList de NaoDocentes.
 * @author João Clara & Paulo Coito;
 */
public class Departamento implements Serializable{
    String nome;
    ArrayList<Curso> cursos;
    ArrayList<Sala> salas;
    ArrayList<Docente> docente;
    ArrayList<NaoDocente> nao_docente;
    /**
     * Inicalizador do Departamento.
     * @param nome (String) Nome do Departamento.
     * Atribui os parametros de entrada às respetivas variáveis. Inicializa todos os ArrayList's.
     */
    public Departamento(String nome)
    {
        this.nome = nome;
        cursos = new ArrayList<Curso>();
        salas = new ArrayList<Sala>();
        docente = new ArrayList<Docente>();
        nao_docente = new ArrayList<NaoDocente>();
    }
    /**
     * Permite adicionar um curso ao Departamento.
     * @param nome. (String) Nome do Curso.
     * @param duracao. (int) Duracao do Curso.
     * @param grau (String) Grau do Curso.
     * Dada a informação necessária à criação de um Curso, adiciona o mesmo ao ArrayList de Curso do Departamento.
     * @return void
     * @see Curso para mais informação acerca do Curso a adicionar.
     */
    private void add_curso(String nome, int duracao, String grau){
        cursos.add(new Curso(nome, duracao, grau));
    }
    /**
     * Permite adicionar uma Sala de exame ao Departamento.
     * @param nome. (String) Nome da Sala.
     * @param max_aluno. (int) Numero maximo de alunos que a sala pode ter.
     * Dada a informação necessária à criação de uma Sala, adiciona a mesma ao ArrayList de salas do Departamento.
     * @return void
     * @see Sala para mais informação acerca do Curso a adicionar.
     */
    private void add_sala_exame(String nome, int max_aluno){
        salas.add(new Sala(nome, max_aluno));
    }
    /**
     * Permite adicionar Docente ao Departamento.
     * @param nome. (String) Nome do Docente.
     * @param email. (String) Email do Docente.
     * @param num_mec. (int) Número Mecanografico do Docente.
     * @param categoria. (String) Categoria do Docente.
     * @param area_invest. (String) Área de investigação do Docente.
     * Dada a informação necessária à criação de um Docente, adiciona o mesmo ao ArrayList de Docente do Departamento.
     * @return void
     * @see Docente para mais informação acerca do Docente a adicionar.
     */
    private void add_docente(String nome, String email, int num_mec, String categoria, String area_invest){
        docente.add(new Docente(nome, email, num_mec, categoria, area_invest));
    }
    /**
     * Permite adicionar um Funcionaro NaoDocente ao Departamento.
     * @param nome. (String) Nome do NaoDocente.
     * @param email. (String) Email do NaoDocente.
     * @param num_mec. (int) Número Mecanografico do NaoDocente.
     * @param categoria. (String) Categoria do NaoDocente.
     * @param cargo. (String) Cargo do NaoDocente.
     * Dada a informação necessária à criação de um NaoDocente, adiciona o mesmo ao ArrayList de NaoDocentes do Departamento.
     * @return void
     * @see NaoDocente para mais informação acerca do NaoDocente a adicionar.
     */
    private void add_nao_docente(String nome, String email, int num_mec, String categoria, String cargo){
        nao_docente.add(new NaoDocente(nome, email, num_mec, categoria, cargo));
    }
    /**
     * Permite listar os cursos de um Departamento.
     * Imprime apenas o nome do curso.
     * @return numero total de curso do Departamento.
     */
    public int imprime_cursos(){
        int i;
        if(this.cursos.isEmpty())
        {
            System.out.println("Nao existem cursos no departamento.");
            return 0;
        }
        System.out.println("Lista de cursos:");
        for (i = 0; i<this.cursos.size(); i++) {
            System.out.println(i+1+" - "+this.cursos.get(i).nome+";");
        }
        return i;
    }
    /**
     * Permite listar todas as salas de um Departamento.
     * Imprime apenas o nome da sala.
     * @return numero total de curso do Departamento.
     */
    public int imprime_salas()
    {
        int i;
        if(this.salas.isEmpty())
        {
            System.out.println("Nao existem salas para exame no departamento.");
            return 0;
        }
        
        System.out.println("Lista de salas:");
        for (i = 0; i<this.salas.size(); i++) {
            System.out.println(i+1+" - "+this.salas.get(i).nome+";");
        }
        return i;
    }
     /**
     * Permite listar todos os não docentes de um Departamento.
     * Imprime apenas o nome do não docente e o seu numero mecanográfico.
     * @return numero total de não docentes do Departamento.
     */
    public int imprime_nao_docentes()
    {
        int i;
        if(this.nao_docente.isEmpty())
        {
            System.out.println("Nao existem nao docentes no departamento.");
            return 0;
        }
        
        System.out.println("Lista de não docentes:");
        for (i = 0; i<this.nao_docente.size(); i++) {
            System.out.println(i+1+" - "+this.nao_docente.get(i)+";");
        }
        return i;
    }
    /**
     * Permite listar todos os docentes de um Departamento.
     * Imprime apenas o nome do docente e o seu numero mecanográfico.
     * @return numero total de docentes do Departamento.
     */
    public int imprime_docentes()
    {
        int i;
        if(this.docente.isEmpty())
        {
            System.out.println("Nao existem nao docentes no departamento.");
            return 0;
        }
        
        System.out.println("Lista de docentes:");
        for (i = 0; i<this.docente.size(); i++) {
            System.out.println(i+1+" - "+this.docente.get(i)+";");
        }
        return i;
    }
}
