package projeto;

import java.io.Serializable;
/**
 * Contem informação de uma Pessoa. 
 * Esta informação é composta por um nome (String) e um email (String).
 * @author João Clara & Paulo Coito
 */
public class Pessoa  implements Serializable{
    String nome;
    String email;
    /**
     * Inicalizador da Pessoa.
     * @param nome (String) Nome da Pessoa.
     * @param email (String) Email da Pessoa.
     */
    public Pessoa(String nome, String email){
        this.nome = nome;
        this.email = email;
    }
    public String getName(){
        return this.nome;
    }
}
