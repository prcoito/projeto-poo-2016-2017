package projeto;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * Contem informação de uma sala. 
 * Esta informação é composta pelo nome da sala, um ArrayList de Exames e o numero maximo de alunos que a sala pode ter.
 * @author João Clara & Paulo Coito;
 */
public class Sala  implements Serializable{
    String nome;
    ArrayList<Exame> exames;
    int max_alunos;
    /**
     * Inicalizador da Sala.
     * @param nome (String) Nome da Sala.
     * @param max (int) Número maximo de alunos que a sala pode ter.
     * Atribui os parametros de entrada às respetivas variáveis. Inicializa todos os ArrayList's.
     */
    public Sala(String nome, int max)
    {
        this.nome = nome;
        this.max_alunos = max;
        this.exames = new ArrayList<Exame>();
    }
    /**
    * Este método permite obter o numero maximo de alunos que uma sala pode ter
    * @return      retorna o numero maximo de alunos que a sala pode ter.
    */
    public int get_num_max_alunos()
    {
        
        return this.max_alunos;
    }
    /**
     * Verifica a disponibilidade da sala numa determinada data.
     * Dado um exame, verifica se esse exame pode ser efetuado nessa sala.
     * É percorrido o ArrayList de exames da sala em que está ocupada, e verifica se a data do exame a adicionar se sobrepõe a alguma data.
     * @param e (Exame) Exame a adicionar.
     * @return boolean. Se a sala estiver retorna true. Caso contrário retorna false.
     */
    public boolean verifica_disponibilidade(Exame e)
    {
        Data d = e.data;
        double duracao = e.duracao;
        double fPart = (duracao % 1);
        short iPart = (short) (duracao - fPart);
        
        int min = (int)(60*fPart) ;
        int fim_exame_a_adicionar = d.hora + (int)( 100*iPart + min);
        if((fim_exame_a_adicionar%100) > 60)
        {
            fim_exame_a_adicionar = fim_exame_a_adicionar-60;   // fica com os minutos bem
            fim_exame_a_adicionar = fim_exame_a_adicionar+100;  // aumenta 1 hora
        }
        int inicio_exame_a_adicionar = d.hora;
        for (Exame exame : this.exames) {
            if(exame.data.ano == d.ano && exame.data.dia == d.dia && exame.data.mes == d.mes)
            {
                duracao = e.duracao;
                fPart = (duracao % 1);
                iPart = (short) (duracao - fPart);

                min = (int)(60*fPart) ;
                int fim_exame_atual = exame.data.hora +(int)( 100*iPart + min);
                if(fim_exame_atual%100 > 60)
                {
                    fim_exame_atual = fim_exame_atual-60;   // fica com os minutos bem
                    fim_exame_atual = fim_exame_atual+100;  // aumenta 1 hora
                }
                  // inicio_exame_atual
                if(! (exame.data.hora > fim_exame_a_adicionar || fim_exame_atual<inicio_exame_a_adicionar) )    
                    return false;
            }
        }
        return true;   
    }
    /**
     * Permite adicionar um exame a uma sala.
     * Dado um exame, adiciona o mesmo ao fim da lista de exames da sala.
     * @param e. (Exame) Exame a adicionar.
     * @return void.
     */
    private void add_exame(Exame e)
    {
        this.exames.add(e);
    }
}
