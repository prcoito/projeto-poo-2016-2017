package projeto;

import java.io.Serializable;

/**
 * Contem informação de uma Data. 
 * Esta informação é constituida por um dia, um mes, um ano e uma hora 
 * Exemplo de uma hora 1000->10H00
 * @author João Clara & Paulo Coito;
 */
public class Data  implements Serializable{
    int dia, mes,ano, hora;
    /**
     * Inicalizador da Data.
     * @param dia (int) Dia
     * @param mes (int) Mes
     * @param ano (int) Ano
     * @param hora (int) Hora
     * Atribui os parametros de entrada às respetivas variáveis.
     */
    public Data(int dia, int mes, int ano, int hora)
    {
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
        this.hora = hora;
    }
    /**
     * Permite saber a hora da Data
     * @return hora da data.
     */
    private int get_hora()
    {
        return this.hora/100;
    }
    /**
     * Permite saber os minutos da Data
     * @return minutos da data.
     */
    private int get_min()
    {
        return this.hora%100;
    }
    
    /**
     * Permite imprimir a data formatada em dia/mes/ano hora( hor:min ).
     * @return String formatada.
     */
    @Override
    public String toString()
    {
        int hor = this.get_hora();
        int min = this.get_min();
        if(min<10)
            return String.format(this.dia+"/"+this.mes+"/"+this.ano+" às "+hor+":0"+min+"\n");
        else
            return String.format(this.dia+"/"+this.mes+"/"+this.ano+" às "+hor+":"+min+"\n");
    }
}
