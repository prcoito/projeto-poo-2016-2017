package projeto;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Contem informação de uma Disciplina. 
 * Esta informação é composta pelo nome da Disciplina, um ArrayList de Docentes, um ArrayList de Alunos e um ArrayList de Exames.
 * @author João Clara & Paulo Coito;
 */
public class Disciplina implements Serializable {
    String nome;
    ArrayList<Docente> Docente;
    ArrayList<Aluno> Aluno;
    public ArrayList<Exame> exames;
    /**
     * Inicalizador da Disciplina.
     * @param nome (String) Nome da Disciplina.
     * Atribui os parametros de entrada às respetivas variáveis. Inicializa todos os ArrayList's.
     */
    public Disciplina(String nome)
    {
        this.nome = nome;
        this.Aluno = new ArrayList<Aluno>();
        this.Docente = new ArrayList<Docente>();
        this.exames = new ArrayList<Exame>();
    }
    /**
     * Permite adicionar Docentes a uma Disciplina
     * @param d (Docente) Docente a adicionar
     * Os docentes adicionados são colocados por ordem alfabetica.
     */
    public void add_docente(Docente d)
    {
        for (int i=0; i<Aluno.size();i++)   // add Docente por ordem alfabetica
        {
            if(d.nome.compareTo(Docente.get(i).nome)<0)
            {
                this.Docente.add(i, d);
                return;
            }
        }
        Docente.add(d);
    }
    /**
     * Permite adicionar Aluno a uma Disciplina
     * @param a (Aluno) Aluno a adicionar
     * Os Alunos adicionados são colocados por ordem alfabetica.
     */
    public void add_aluno(Aluno a)
    {
        for (int i=0; i<Aluno.size();i++)   // add aluno por ordem alfabetica
        {
            if(a.nome.compareTo(Aluno.get(i).nome)<0)
            {
                this.Aluno.add(i, a);
                return;
            }
        }
        this.Aluno.add(a);
    }
    /**
     * Permite imprimir todos os exames da Disciplina.
     * Caso não exitam exames informa utilizador que nao existem exames.
     * Imprime os exames pelo seu tipo (Normal/Recurso/Especial)
     * @return numero total de exames
     */
    public int imprime_exames()
    {
        int i;
        if(this.exames.isEmpty())
        {
            System.out.println("Não existem exames para esta disciplina\n");
            return 0;
        }
        System.out.print("Lista de exames:\n");
        for (i = 0; i<this.exames.size(); i++) {
            System.out.println(i+1+" - "+this.exames.get(i).tipo()+";");
        }
        return i;
    }
    @Override
    public String toString()
    {
        return String.format(this.nome);
    }
}
